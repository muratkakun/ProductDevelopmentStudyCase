﻿using AutoMapper;
using Data;
using Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class AnimalService : IAnimalService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public AnimalService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Update(Guid id, AnimalDto model)
        {
            return await _unitOfWork.Animals.Update(id, _mapper.Map<Animal>(model)) != null;
        }

        public async Task<bool> Add(AnimalDto model)
        {
            return await _unitOfWork.Animals.Add(_mapper.Map<Animal>(model)) != null;
        }

        public async Task<List<AnimalDto>> GetAll()
        {
            var results = await _unitOfWork.Animals.GetAll();
            return _mapper.Map<List<AnimalDto>>(results);
        }

        public async Task<PaginatedListDto<AnimalDto>> GetByPage(int pageIndex = 0, int pageCount = 50)
        {
            var results = _unitOfWork.Animals.Get();
            var page = new PaginatedListDto<Animal>(results, pageIndex, pageCount);
            var maped = _mapper.Map<List<AnimalDto>>(page.Data);

            return new PaginatedListDto<AnimalDto>(maped, pageIndex, pageCount);
        }

        public async Task<AnimalDto> GetById(Guid id)
        {
            return _mapper.Map<AnimalDto>(await _unitOfWork.Animals.GetById(id));
        }

        public async Task<bool> Delete(Guid id)
        {
            using (var transection = await _unitOfWork.BeginTransactionAsync())
            {
                await _unitOfWork.Animals.Remove(id);
                var visits = await _unitOfWork.Visits.GetVisitsByAnimalId(id);
                foreach (var visit in visits)
                {
                   await _unitOfWork.Visits.Remove (visit.Id);
                }

                await transection.CommitAsync();
            }

            return true;
        }
    }
}
