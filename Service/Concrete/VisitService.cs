﻿using AutoMapper;
using Data;
using Microsoft.EntityFrameworkCore;
using Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class VisitService : IVisitService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public VisitService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> Update(Guid id, VisitDto model)
        {
            return await _unitOfWork.Visits.Update(id, _mapper.Map<Visit>(model)) != null;
        }
        
        private IQueryable<VisitDto> GetAllQuery()
        {
            return (from v in _unitOfWork.Visits.Get()
                        join a in _unitOfWork.Animals.Get() on v.AnimalId equals a.Id into aa
                        from av in aa
                        select new VisitDto
                        {
                            Id = v.Id,
                            AnimalId = v.AnimalId,
                            DeviceId = v.DeviceId,
                            FeedingDate = v.FeedingDate,
                            Intake = v.Intake,
                            Animal = new AnimalDto
                            {
                                BirthDate = av.BirthDate,
                                Description = av.Description,
                                FatherLifeNumber = av.FatherLifeNumber,
                                Gender = av.Gender,
                                Id = av.Id,
                                LifeNumber = av.LifeNumber,
                                MotherLifeNumber = av.MotherLifeNumber,
                                Name = av.Name
                            }
                        }
                        );
        }

        public async Task<List<VisitDto>> GetAll()
        {        
            return await GetAllQuery().ToListAsync();
        }

        public async Task<VisitDto> GetById(Guid id)
        {
            var visit = await _unitOfWork.Visits.GetById(id);
            var result = _mapper.Map<VisitDto>(visit);
            result.Animal = _mapper.Map<AnimalDto>(await _unitOfWork.Animals.GetById(visit.AnimalId));
            return result;
        }

        public async Task<bool> Add(VisitDto model)
        {
            return await _unitOfWork.Visits.Add(_mapper.Map<Visit>(model)) == null;
        }

        public async Task<bool> Delete(Guid id)
        {
            return await _unitOfWork.Animals.Remove(id);
        }

        public async Task<PaginatedListDto<VisitDto>> GetByPage(int pageIndex = 0, int pageCount = 50)
        {
            var results = GetAllQuery();
            var page = new PaginatedListDto<VisitDto>(results, pageIndex, pageCount);
            
            return page;
        }
    }
}
