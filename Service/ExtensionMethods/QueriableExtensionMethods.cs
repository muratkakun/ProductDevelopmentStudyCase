﻿using System.Linq;

namespace Api.Common
{
    public static class QueriableExtensionMethods
    {
        public static IQueryable<T> Paginate<T>(this IQueryable<T> data, int pageIndex, int pageCount)
        {
            var page = data.Skip(pageIndex * pageCount).Take(pageCount);
            return page;
        }
    }
}
