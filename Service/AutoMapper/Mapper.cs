﻿using AutoMapper;
using Data;
using Service.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.AutoMapper
{
   public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Animal, AnimalDto>();
            CreateMap<AnimalDto, Animal>();
            CreateMap<PaginatedListDto<Animal>, PaginatedListDto<AnimalDto>>();

            CreateMap<Visit, VisitDto>();
            CreateMap< PaginatedListDto< Visit>, PaginatedListDto<VisitDto>>();
            CreateMap<VisitDto, Visit>();
        }
    }
}
