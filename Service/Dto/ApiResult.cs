﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Dto
{
   public class ApiResult<T>
    {
        public ApiResult(T data, bool success, string message)
        {
            Result = data;
            Success = success;
            Message = message;
        }
        public bool Success { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }
}
