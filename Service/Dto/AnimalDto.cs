﻿using System;

namespace Service
{
    public class AnimalDto : BaseDto
    {
        public string LifeNumber { get; set; }
        public string Name { get; set; }
        public Int16 Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string FatherLifeNumber { get; set; }
        public string MotherLifeNumber { get; set; }
        public string Description { get; set; }
    }
}
