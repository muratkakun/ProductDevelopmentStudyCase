﻿using Api.Common;
using System.Collections.Generic;
using System.Linq;

namespace Service.Dto
{
    public class PaginatedListDto<T>
    {
        public int PageIndex { get; set; }
        public int PageCount { get; set; }
        public int TotalCount { get; set; }
        public List<T> Data { get; set; }
        public PaginatedListDto(List<T> data, int pageIndex, int pageCount)
        {
            TotalCount = data.Count();
            PageIndex = pageIndex;
            PageCount = pageCount;
            Data = data;
        }
        public PaginatedListDto(IQueryable<T> data, int pageIndex, int pageCount)
        {
            TotalCount = data.Count();
            PageIndex = pageIndex;
            PageCount = pageCount;
            Data = data.Paginate(pageIndex, pageCount).ToList<T>();
        }
    }
}
