﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service
{
    public class VisitDto
    {
        public Guid Id { get; set; }

        public Guid DeviceId { get; set; }

        public Guid AnimalId { get; set; }

        public DateTime FeedingDate { get; set; }
        public int Intake { get; set; }

        public AnimalDto Animal { get; set; }
    }
}
