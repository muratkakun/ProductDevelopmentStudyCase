﻿using Data;
using Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
   public interface IBaseService<T>
    {
        Task<PaginatedListDto<T>> GetByPage(int pageIndex = 0, int pageCount = 50);
        Task<List<T>> GetAll();
        Task<T> GetById(Guid id);
        Task<bool> Update(Guid id, T model);
        Task<bool> Add(T model);
        Task<bool> Delete(Guid id);

    }
}
