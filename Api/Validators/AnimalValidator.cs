﻿using FluentValidation;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Validators
{
    public class AnimalValidator : AbstractValidator<AnimalDto>
    {
        public AnimalValidator()
        {
            RuleFor(x => x.Name).Length(1, 10);
            RuleFor(x => x.LifeNumber).Length(1, 10);
            RuleFor(x => x.MotherLifeNumber).Length(1, 10);
            RuleFor(x => x.Gender).NotNull().IsInEnum();
        }

    }
}
