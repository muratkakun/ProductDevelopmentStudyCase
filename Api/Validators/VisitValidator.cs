﻿using FluentValidation;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Validators
{
    public class VisitValidator:AbstractValidator<VisitDto>
    {
        public VisitValidator()
        {
            RuleFor(x => x.AnimalId).NotNull().NotEmpty();
            RuleFor(x => x.DeviceId).NotNull().NotEmpty();
            RuleFor(x => x.Intake).ExclusiveBetween(0, 100);
        }
    }
}
