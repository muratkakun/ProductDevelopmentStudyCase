using Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ProductDevelopmentStudyCase
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string dbName = "ProductDeveloppmentDB.db";
            if (File.Exists(dbName))
            {
                File.Delete(dbName);
            }
            using (var dbContext = new SqlLiteContext())
            {
                //Ensure database is created
                dbContext.Database.EnsureCreated();
                if (!dbContext.Animals.Any())
                {
                    dbContext.Animals.AddRange(new Animal[]
                        {
                             new Animal{ BirthDate = DateTime.Now, Description = "My lovely cow",
                              FatherLifeNumber = "NL234567", Gender = 0, LifeNumber = "NL123456", MotherLifeNumber="NL234568", Name = "Lily", Id = Guid.NewGuid()}
                        });
                    dbContext.SaveChanges();
                }
            }
            CreateHostBuilder(args).Build().Run();
           
            Console.ReadLine();

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
