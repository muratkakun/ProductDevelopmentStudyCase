﻿using Microsoft.AspNetCore.Http;
using Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;

namespace Api.Common.Middleware
{
    public class ErrorHandlingMidleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingMidleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception error)
            {
                var response = context.Response;
                response.ContentType = "application/json";

                switch (error)
                {
                    case BadHttpRequestException:
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        break;
                    case KeyNotFoundException:
                        response.StatusCode = (int)HttpStatusCode.NotFound;
                        break;
                    default:
                        response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        break;
                }

                var apiResult = new ApiResult<bool>(false, false, "Error occured on api. Error message : " + error?.Message);
                var result = JsonSerializer.Serialize(apiResult);
                await response.WriteAsync(result);
            }
        }
    }
}
