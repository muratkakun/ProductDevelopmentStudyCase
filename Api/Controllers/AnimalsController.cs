﻿using Api.Controllers.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service;
using Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductDevelopmentStudyCase.Controllers
{
    public class AnimalsController : BaseController<AnimalDto>
    {
        private readonly IAnimalService _animalService;
        public AnimalsController(IAnimalService animalService)
        {
            _animalService = animalService;
        }
        /// <summary>
        /// Deletes an animal.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Deletes specific animal</returns>
        /// <response code="200">Returns ApiResult
        /// <remarks>
        /// Sample response:
        ///     ```{
        ///          "Result" = true;
        ///          "Success" = true;
        ///          "Message" = "";
        ///     }```
        /// </remarks>
        /// </response>

        public override async Task<ApiResult<bool>> Delete(Guid id)
        {
            var isDeleted = await _animalService.Delete(id);

            return new ApiResult<bool>(isDeleted, isDeleted, "Deleted successfully");
        }

        /// <summary>
        /// Gets all animals.
        /// </summary>
        /// <returns>ApiResult object with all animal data</returns>
        public async override Task<ApiResult<List<AnimalDto>>> GetAll()
        {
            var result = await _animalService.GetAll();

            return new ApiResult<List<AnimalDto>>(result, result != null, "Ok");
        }

        /// <summary>
        /// Gets all animals by pagination.
        /// </summary>
        /// <returns>ApiResult object with all animal data by pagination</returns>
        public async override Task<ApiResult<PaginatedListDto<AnimalDto>>> GetByPage(int pageIndex = 0, int pageCount = 50)
        {
            var result = await _animalService.GetByPage();

            return new ApiResult<PaginatedListDto<AnimalDto>>(result, result != null, "Ok");
        }

        /// <summary>
        /// Gets specific animal with id.
        /// </summary>
        /// <returns>ApiResult object with all animal data</returns>
        /// <param name="id"></param>
        public async override Task<ApiResult<AnimalDto>> GetById(Guid id)
        {
            var result = await _animalService.GetById(id);

            return new ApiResult<AnimalDto>(result, result != null, "Ok");
        }

        /// <summary>
        /// Creates new animal.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>creates new animal
        /// </returns>
        /// <remarks>
        /// ```
        /// Sample request:
        ///     Post /animals
        ///     {
        ///         "LifeNumber": "NL123456",
        ///         "Name": "Lily",
        ///         "Gender": "Female",
        ///         "BirthDate": "2021-06-30T11:00:00.000Z",
        ///         "FatherLifeNumber": "NL234567",
        ///         "MotherLifeNumber": "NL234568",
        ///         "Description": "My lovely cow"
        ///     }
        /// </remarks>
        /// <response code="200">Returns ApiResult
        /// <remarks>
        /// Sample response:
        ///     ```{
        ///          "Result" = true;
        ///          "Success" = true;
        ///          "Message" = "";
        ///     }```
        /// </remarks>
        /// </response>
        public async override Task<ApiResult<bool>> Insert([FromBody] AnimalDto model)
        {
            var result = await _animalService.Add(model);
            return new ApiResult<bool>(result, result,"Inserted successfuly");
        }

        /// <summary>
        /// Updates specific animal.
        /// </summary>
        /// <returns>ApiResult object with success status</returns>
        /// <param name="id"></param>
        /// <param name="model"></param>
        public async override Task<ApiResult<bool>> Update([FromRoute] Guid id, [FromBody] AnimalDto model)
        {
            var result = await _animalService.Update(id, model);
            return new ApiResult<bool>(result, result, "Updatede successfuly");
        }
    }
}
