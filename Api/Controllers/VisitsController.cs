﻿using Api.Controllers.Base;
using Microsoft.AspNetCore.Mvc;
using Service;
using Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
   
    public class VisitsController : BaseController<VisitDto>
    {
        private readonly IVisitService _visitService;
        public VisitsController(IVisitService visitService)
        {
            _visitService = visitService;
        }
        public override async Task<ApiResult<bool>> Delete(Guid id)
        {
            var isDeleted = await _visitService.Delete(id);

            return new ApiResult<bool>(isDeleted, isDeleted, null);
        }

        public async override Task<ApiResult<List<VisitDto>>> GetAll()
        {
            var result = await _visitService.GetAll();

            return new ApiResult<List<VisitDto>>(result, result != null, null);
        }

        public async override Task<ApiResult<VisitDto>> GetById(Guid id)
        {
            var result = await _visitService.GetById(id);

            return new ApiResult<VisitDto>(result, result != null, null);
        }

        public async override Task<ApiResult<PaginatedListDto<VisitDto>>> GetByPage(int pageIndex = 0, int pageCount = 50)
        {
            var result = await _visitService.GetByPage();

            return new ApiResult<PaginatedListDto<VisitDto>>(result, result != null, "Ok");
        }

        public async override Task<ApiResult<bool>> Insert([FromBody] VisitDto model)
        {
            var result = await _visitService.Add(model);
            return new ApiResult<bool>(result, result, null);
        }

        public async override Task<ApiResult<bool>> Update([FromRoute] Guid id, [FromBody] VisitDto model)
        {
            var result = await _visitService.Update(id, model);
            return new ApiResult<bool>(result, result, null);
        }
    }
}
