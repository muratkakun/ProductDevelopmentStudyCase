﻿using Microsoft.AspNetCore.Mvc;
using Service.Dto;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers.Base
{
  
    [ApiController]
    [Route("api/[Controller]")]
    public abstract class BaseController<T> : ControllerBase
    {
        [Produces("application/json")]
        [HttpGet]
        [Route("{id}")]
        public abstract Task<ApiResult<T>> GetById(Guid id);

        [Produces("application/json")]
        [HttpGet]
        public abstract Task<ApiResult<List<T>>> GetAll();

        [Produces("application/json")]
        [HttpGet]
        [Route("Paginated")]
        public abstract Task<ApiResult<PaginatedListDto<T>>> GetByPage(int pageIndex = 0, int pageCount = 50);

        [Produces("application/json")]
        [HttpPost]
        public abstract Task<ApiResult<bool>> Insert([FromBody] T model);

        [Produces("application/json")]
        [HttpPut]
        [Route("{id}")]
        public abstract Task<ApiResult<bool>> Update([FromRoute] Guid id, [FromBody] T model);

        [Produces("application/json")]
        [HttpDelete]
        [Route("{id}")]
        public abstract Task<ApiResult<bool>> Delete(Guid id);
    }
}
