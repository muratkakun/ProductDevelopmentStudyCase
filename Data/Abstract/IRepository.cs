﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface IRepository<T> where T : class, IEntity
    {
        IQueryable<T> Get();
        Task<List<T>> GetAll();
        Task<T> GetById(Guid id);
        Task<T> Update(Guid id, T entity);
        Task<T> Add(T entity);
        Task<bool> Remove(Guid id);
    }
}
