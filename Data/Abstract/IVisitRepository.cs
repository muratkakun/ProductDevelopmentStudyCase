﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public interface IVisitRepository : IRepository<Visit>
    {
        Task<List<Visit>> GetVisitsByAnimalId(Guid animalId);
    }
}
