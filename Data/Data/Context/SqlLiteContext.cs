﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;

namespace Data
{
    public class SqlLiteContext : DbContext
    {
        public string DbPath { get; }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public SqlLiteContext() : base()
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = System.IO.Path.Join(path, "ProductDeveloppmentDB.db");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={DbPath}", options =>
            {
                options.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName);
            });
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Animal>().ToTable("Animal", "Common");
            modelBuilder.Entity<Animal>().HasKey(p => p.Id);

            modelBuilder.Entity<Animal>()
                        .Property(p => p.BirthDate)
                        .IsRequired();
            modelBuilder.Entity<Animal>().Property(e => e.CreateDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

            modelBuilder.Entity<Animal>()
                       .Property(p => p.FatherLifeNumber)
                       .IsRequired()
                       .HasMaxLength(10)
                       .HasColumnType("nvarchar(10)");

            modelBuilder.Entity<Animal>()
                      .Property(p => p.Gender)
                      .IsRequired();

            modelBuilder.Entity<Animal>()
                   .Property(p => p.Description)
                   .IsRequired()
                   .HasMaxLength(100).HasColumnType("nvarchar(100)");

            modelBuilder.Entity<Visit>().ToTable("Visit", "Common");
            modelBuilder.Entity<Visit>().Property(e => e.CreateDate).HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Visit>().HasKey(p => p.Id);
            modelBuilder.Entity<Visit>()
                     .Property(p => p.Intake)
                     .IsRequired();

            modelBuilder.Entity<Visit>().HasOne(x => x.Animal);
            modelBuilder.Entity<Visit>().Navigation(x => x.Animal);
            modelBuilder.Entity<Visit>()
                     .Property(p => p.DeviceId)
                     .IsRequired();

            modelBuilder.Entity<Visit>()
             .HasOne(d => d.Animal)
             .WithMany(dm => dm.Visits)
             .HasForeignKey(dkey => dkey.AnimalId);

            base.OnModelCreating(modelBuilder);
                }
    }
}
