﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data
{
    public class Animal : IEntity
    {
        public Guid Id { get; set; }
        public string LifeNumber { get; set; }
        public string Name { get; set; }
        public Int16 Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public string FatherLifeNumber { get; set; }
        public string MotherLifeNumber { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }

        public virtual List<Visit> Visits { get; set; }
    }
}
