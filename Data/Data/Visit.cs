﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data
{
    public class Visit : IEntity
    {
        public Guid Id { get; set; }

        public Guid DeviceId { get; set; }

        [ForeignKey("Animal")]
        public Guid AnimalId { get; set; }

        public DateTime FeedingDate { get; set; }
        public int Intake { get; set; }

        public DateTime CreateDate { get; set; }

        public virtual Animal Animal { get; set; }
    }
}
