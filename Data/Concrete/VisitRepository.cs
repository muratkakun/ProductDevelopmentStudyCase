﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class VisitRepository : EfCoreRepository<Visit, SqlLiteContext>, IVisitRepository
    {
        public VisitRepository(SqlLiteContext context):base(context)
        {
           
        }

        public async Task<List<Visit>> GetVisitsByAnimalId(Guid animalId)
        {
            return await _context.Visits.AsNoTracking().Where(x => x.AnimalId == animalId).ToListAsync();
        }

    }
}
