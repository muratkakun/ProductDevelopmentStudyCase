﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class AnimalRepository : EfCoreRepository<Animal, SqlLiteContext>, IAnimalRepository
    {
        public AnimalRepository(SqlLiteContext context) : base(context)
        {

        }
    }
}
